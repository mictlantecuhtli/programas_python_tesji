#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 9-09-2022                  #
#Autor: Fatima Azucena MC           #
# fatimaazucenamartinez274@gmail.com#
#####################################

numero = 30
doble = numero * 2
tercera = doble / 3
mitad = tercera / 2

print("La mitad de la tercera partede del doble de 30 es: ", mitad)
