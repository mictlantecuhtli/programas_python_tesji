#Fecha: 18-08-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

n=int(input("Digite hasta que número desea ver su factorial: "))

a=1
fact=1

while a <= n:
    b = (a*fact)
    print(fact, " x ", a , " = ", b )
    fact = (fact*a)
    a = a + 1

