#Fecha: 18-08-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

tabla=int(input("Digite que tabla desea ver: "))
tope=int(input("¿Hasta donde desea visualizar: "))

a=1

while a <= tope:
    resultado = (tabla*a)
    print(tabla," x ", a," = ", resultado)
    a = a + 1

