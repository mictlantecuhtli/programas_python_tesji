#Fecha: 18-08-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

n=int(input("Digite un número: "))

a=1
suma=0

while a <= n:
    suma = (suma + a)
    a = a + 1

print("El resultado de la suma es: " , suma)

