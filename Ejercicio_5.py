#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 10-11-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

print("Tallas disponibles")
print("1.- Talla chica")
print("2.- Talla mediana")
print("3.- Talla grande")
opcion=int(input("Digite su opción: "))
total=0

if opcion == 1:
    print("A elegido talla chica (Talla 7)")
    cantidad=int(input("¿Cuántos vestidos compró?: "))
    total = (cantidad*300)
if opcion == 2:
    print("A elegido talla mediana (Talla 12)")
    cantidad=int(input("¿Cuántos vestidos compró?: "))
    total = (cantidad*400)
if opcion == 3:
    print("A elegido talla grande (Talla 16)")
    cantidad=int(input("¿Cuántos vestidos compró?: "))
    total = (cantidad*500)

print("El total a pagar es: ", total)
