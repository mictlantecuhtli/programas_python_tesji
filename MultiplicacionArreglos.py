#!/usr/local/bin/python
# -*- coding: utf-8 -*-
import os, sys

#####################################
#Fecha: 06-10-2022                  #
#Autor: Fatima Azucena MC           #
#<fatimaazucenamartinez274@gmail.com#
#####################################

a1=[]
a2=[]
result = 0
num1 = 0
num2 = 0
b = 4

print("Arreglo 1")

for i  in range (5) :
    print("Digite el número en la posición " , i , ": ")
    num1 = int(input())
    a1.append(num1)


print(" ")
for y in range (5): 
    print(a1[y])


print(" ")
print("Arreglo 2")

for a  in range (5) :
    print("Digite el número en la posición " , a , ": ")
    num2 = int(input())
    a2.append(num2)


print(" ")
for b in range (5): 
    print(a2[b])


print(" ")
print("Resultado")
for z in range (5):
    result = a1[z]*a2[b]
    b = b - 1
    print(result)

